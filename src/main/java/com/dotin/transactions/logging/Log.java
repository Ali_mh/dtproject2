package com.dotin.transactions.logging;

import com.dotin.transactions.jsonutility.JsonParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Log {

    public static void createLog(String logContext) {
        JsonParser jsonParser = new JsonParser();
        jsonParser.readFile("src/main/resources/core.json");
        String path = jsonParser.getLogPath();
        try {
            FileWriter fileWriter = new FileWriter(path, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            String temp = logContext;
            bufferedWriter.write(temp);
            bufferedWriter.newLine();
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}



