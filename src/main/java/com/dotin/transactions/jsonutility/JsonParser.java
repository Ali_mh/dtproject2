package com.dotin.transactions.jsonutility;

import com.dotin.transactions.deposit.Deposit;
import com.dotin.transactions.errorhandling.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class JsonParser {
    private Integer port;
    private String logPath;


    public List<Deposit> readFile(String filePath) {
        List<Deposit> depositList = new ArrayList<Deposit>();
        try {
            FileReader reader = new FileReader(filePath);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            port = Integer.valueOf(jsonObject.get("port").toString());
            logPath = jsonObject.get("outLog").toString();
            JSONArray deposits = (JSONArray) jsonObject.get("deposits");
            for (int i = 0; i < deposits.size(); i++) {
                JSONObject innerObj = (JSONObject) deposits.get(i);
                Deposit deposit = new Deposit();

                String customerName;
                Integer customerId;
                BigDecimal initialBalance;
                Long upperBound;
                String customerIdString = innerObj.get("id").toString();
                try {
                    customerName = innerObj.get("customer").toString();
                    customerId = Integer.valueOf(innerObj.get("id").toString());
                    initialBalance = new BigDecimal(innerObj.get("initialBalance").toString());
                    upperBound = Long.valueOf(innerObj.get("upperBound").toString());
                } catch (Exception e) {
                    System.out.println("Invalid input! CustomerId= " + customerIdString);
                    continue;
                }
                deposit.setCustomerName(customerName);
                deposit.setCustomerId(customerId);
                deposit.setInitialBalance(initialBalance);
                deposit.setUpperBound(upperBound);

                if (ErrorManager.jsonInputError(deposit))
                    continue;
                depositList.add(deposit);
            }


        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        return depositList;
    }

    public Integer getPort() {
        return port;
    }

    public String getLogPath() {
        return logPath;
    }
}
