package com.dotin.transactions.server;

import com.dotin.transactions.deposit.Deposit;
import com.dotin.transactions.deposit.DepositOperator;
import com.dotin.transactions.deposit.WithdrawOperator;
import com.dotin.transactions.logging.Log;
import com.dotin.transactions.transaction.*;

import java.io.*;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class TerminalManager implements Runnable {
    private Socket socket;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:SSS");
    private InputStream inputStream;
    private OutputStream outputStream;

    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    List<Deposit> depositList;
    private Server server;

    public TerminalManager(Socket socket, List<Deposit> depositList, Server server) {
        this.socket = socket;
        this.depositList = depositList;
        this.server = server;

    }

    public void run() {
        server.incLimit();
        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();

            dataInputStream = new DataInputStream(inputStream);
            dataOutputStream = new DataOutputStream(outputStream);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            BigDecimal initialBalance = null;
            boolean i = true;
            while (i) {
                String terminalInfo = dateFormat.format(new Date()) + " " + dataInputStream.readUTF();
                String log = terminalInfo + "\n";
                Transaction transaction = (Transaction) objectInputStream.readObject();
                log += transaction.toString();
                int depositIndex = 0;
                TransactionResult transactionResult = TransactionResult.INVALID_CUSTOMERID;

                for (int j = 0; j < depositList.size(); j++) {
                    if (depositList.get(j).getCustomerId().equals(transaction.getTransactionDeposit())) {
                        synchronized (Deposit.class) {
                            depositIndex = j;
                            initialBalance = depositList.get(j).getInitialBalance();
                            switch (transaction.getTransactionType()) {
                                case deposit:
                                    transactionResult = DepositOperator.operation(depositList.get(j), transaction.getTransactionAmount());
                                    break;
                                case withdraw:
                                    transactionResult = WithdrawOperator.operation(depositList.get(j), transaction.getTransactionAmount());
                                    break;
                            }
                        }

                    }

                }
                BigDecimal depositBalance = depositList.get(depositIndex).getInitialBalance();
                if (transactionResult == TransactionResult.INVALID_CUSTOMERID) {
                    depositBalance = BigDecimal.valueOf(-1);
                    initialBalance = BigDecimal.valueOf(-1);
                }
                Response response = new
                        Response(transaction.getTransactionType(), transactionResult, initialBalance, depositBalance, transaction.getTransactionId(), transaction.getTransactionAmount());

                objectOutputStream.writeObject(response);
                log += "Initial Balance=" + initialBalance + "\n";
                log += (dateFormat.format(new Date()) + " " + transactionResult + " DepositBalance = " + depositBalance);
                Log.createLog(log);
                System.out.println(log);


                i = dataInputStream.readBoolean();
            }

            try {
                dataInputStream.close();
                dataOutputStream.close();
                objectInputStream.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                Log.createLog(dateFormat.format(new Date()) + " " + e.getMessage());
            }


        } catch (Exception e) {
        }
        server.decLimit();
    }


}