package com.dotin.transactions.server;

import com.dotin.transactions.deposit.Deposit;
import com.dotin.transactions.jsonutility.JsonParser;
import com.dotin.transactions.logging.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Server {
    private ServerSocket serverSocket;
    private int serverPort;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:SSS");
    private int threadLimit = 0;

    public Server() {

        try {
            JsonParser jsonParser = new JsonParser();
            List<Deposit> depositList = jsonParser.readFile("src/main/resources/core.json");

            serverPort = jsonParser.getPort();
            serverSocket = new ServerSocket(serverPort);

            int t = 0;
            Log.createLog(dateFormat.format(new Date()) + " Server Created!");
            System.out.println("Server Created!");

            while (true) {

                Socket socket = serverSocket.accept();
                System.out.println("New Client Connected!");
                Log.createLog(dateFormat.format(new Date()) + " New Client Connected! " + socket.getRemoteSocketAddress());
                t++;
                String threadName = "thread" + t;
                Thread thread = new Thread(new TerminalManager(socket, depositList, this), threadName);

                if (threadLimit > 10000)
                    Thread.sleep(1000);
                thread.start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void incLimit() {
        threadLimit++;
    }

    public void decLimit() {
        threadLimit--;
    }

}


