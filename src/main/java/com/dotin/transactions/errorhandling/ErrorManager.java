package com.dotin.transactions.errorhandling;

import com.dotin.transactions.deposit.Deposit;

import java.math.BigDecimal;

public class ErrorManager {
    public static boolean jsonInputError(Deposit deposit) {
        Long upperBound = deposit.getUpperBound();
        BigDecimal initialBalance = deposit.getInitialBalance();
        Integer customerId = deposit.getCustomerId();
        try {
            if (upperBound < 0)
                throw new Exception("UpperBound cannot be negative!CustomerId=" + customerId);
            if (initialBalance.signum() < 0)
                throw new Exception("InitialBalance cannot be negative!CustomerId=" + customerId);
            return false;
        } catch (Throwable e) {
            System.out.println(e.getMessage());
            return true;
        }
    }
}
