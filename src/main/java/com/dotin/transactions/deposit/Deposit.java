package com.dotin.transactions.deposit;


import java.math.BigDecimal;

public class Deposit {
    private Integer customerId;
    private String CustomerName;
    protected Long upperBound;
    protected BigDecimal initialBalance;

    @Override
    public String toString() {
        return "Deposit{" +
                "customerId=" + customerId +
                ", CustomerName='" + CustomerName + '\'' +
                ", upperBound=" + upperBound +
                ", initialBalance=" + initialBalance +
                '}';
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public Long getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(Long upperBound) {
        this.upperBound = upperBound;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

}


