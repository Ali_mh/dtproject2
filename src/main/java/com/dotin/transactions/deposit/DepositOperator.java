package com.dotin.transactions.deposit;

import com.dotin.transactions.transaction.TransactionResult;

import java.math.BigDecimal;


public class DepositOperator {


    public static TransactionResult operation(Deposit deposit, Long amount) {
        BigDecimal initialBalance = deposit.getInitialBalance();
        BigDecimal bigDAmount = BigDecimal.valueOf(amount);
        if (amount <= 0)
            return TransactionResult.INVALID_AMOUNT;
        deposit.setInitialBalance(initialBalance.add(bigDAmount));
        return TransactionResult.SUCCESS_TRANSACTION;

    }

}
