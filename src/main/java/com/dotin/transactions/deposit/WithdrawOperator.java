package com.dotin.transactions.deposit;

import com.dotin.transactions.transaction.TransactionResult;

import java.math.BigDecimal;

public class WithdrawOperator {


    public static TransactionResult operation(Deposit deposit, Long amount) {
        BigDecimal initialBalance = deposit.getInitialBalance();
        BigDecimal bigDAmount = BigDecimal.valueOf(amount);
        if (amount <= 0)
            return TransactionResult.INVALID_AMOUNT;
        if (initialBalance.compareTo(bigDAmount) < 0)
            return TransactionResult.LOW_INITIALBALANCE;
        if (bigDAmount.compareTo(BigDecimal.valueOf(deposit.getUpperBound())) >= 0 && deposit.getUpperBound() != 0)
            return TransactionResult.UPPERBOUND_FAILURE;
        deposit.setInitialBalance(initialBalance.subtract(bigDAmount));
        return TransactionResult.SUCCESS_TRANSACTION;
    }
}
