package com.dotin.transactions.transaction;

import java.io.Serializable;

public class Transaction implements Serializable {
    private Integer transactionId;
    private TransactionType transactionType;
    private Long transactionAmount;
    private Integer transactionDeposit;

    @Override
    public String toString() {
        return "Transaction{" +
                "transId=" + transactionId +
                ", transType=" + transactionType +
                ", transAmount=" + transactionAmount +
                ", transDeposit=" + transactionDeposit +
                '}';
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Long getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Long transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Integer getTransactionDeposit() {
        return transactionDeposit;
    }

    public void setTransactionDeposit(Integer transactionDeposit) {
        this.transactionDeposit = transactionDeposit;
    }
}
