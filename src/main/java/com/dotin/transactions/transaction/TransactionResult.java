package com.dotin.transactions.transaction;


public enum TransactionResult {
    SUCCESS_TRANSACTION(0),
    LOW_INITIALBALANCE(1),
    UPPERBOUND_FAILURE(2),
    INVALID_CUSTOMERID(3),
    INVALID_AMOUNT(4);

    final int index;

    TransactionResult(int index) {
        this.index = index;
    }

    public int getTransactionResultIndex() {
        return index;
    }

    public static void transactionResultValidation(String testTransactionResult) {
        try {
            TransactionResult.valueOf(testTransactionResult);
        } catch (Exception e) {
            System.out.println("Deposit Id does not exist!");
        }

    }

}
